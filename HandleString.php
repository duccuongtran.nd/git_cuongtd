<?php

class HandleString
{
    //property
    public $check1,$check2;
    /**
    * read file
    *
    * @param path file
    * @return string || flase
    */ 
    public function readFile($fileName) {
        $fp = @fopen($fileName, "r");
        if (!$fp) {
            echo "lỗi mở file";
            return false;
        }
        return file_get_contents($fileName);
    }
    /**
    * check string
    *
    * @param string $values
    * @return boolean
    */
    public function checkValidString($values) {
        //check $values is empty
        if ($values === '') {
            return true;
        }
        if (strpos($values, 'after') === false && strlen($values) >=50 ) {
            return true;
        }
        if (strpos($values, 'before') === true && strpos($values, 'after') === false ) {
            return true;
        }
        return false;
    } 
}

$object1 = new HandleString();
if ($object1->readFile('file1.txt') !== false ) {
    $object1->check1 = $object1->checkValidString($object1->readFile('file1.txt'));
}
echo '<br>';
if ($object1->readFile('file2.txt') !== false ) {
    $object1->check2 = $object1->checkValidString($object1>readFile('file2.txt'));
}
?>
