<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Login.php</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <?php
        // Code PHP
        $error = array();
        $data = array();
        if (!empty($_POST['login'])) {
            $data['email'] = $_POST['email'] ?? '';
            $data['password'] = $_POST['password'] ?? '';

            function is_email($email)
            {
                return (!preg_match("/^[a-zA-Z0-9]{6,225}\@[a-zA-Z0-9]{3,20}\.[a-zA-Z]{2,5}$/", $email)) ? FALSE : TRUE;
            }
            function is_password($password)
            {
                return (!preg_match("/^[a-zA-Z0-9]{6,50}$/", $password)) ? FALSE : TRUE;
            }
            if (empty($data['email'])) {
                $error['email'] = 'Bạn chưa nhập email';
            } elseif (!is_email($data['email'])) {
                $error['email'] = 'Email không đúng định dạng';
            }
            if (empty($data['password'])) {
                $error['password'] = 'Bạn chưa nhập Password';
            } elseif (!is_password($data['password'])) {
                $error['password'] = 'Password không đúng định dạng';
            }
            if (!$error) {
                $email = $_POST["email"];
                $password = $_POST["password"];
                if ($email == "cuongtd@gmail.com" && $password == "cuongtd123") {
                    $_SESSION['email'] = $email;
                    if (isset($_POST['check'])) {
                        setcookie('email', $email, time() + 3600);
                        setcookie('pass', $password, time() + 3600);
                    }
                    header("location:LoginSuccess.php");
                } else {
                    echo 'Đăng nhập thất bại';
                }
            } else {
                echo 'Dữ liệu bị lỗi, không thể lưu trữ';
            }
        }
        ?>
        <h1>Login.php</h1>
        <form method="post" action="Login.php">
            <table cellspacing="0" cellpadding="5">
                <tr>
                    <td>Email của bạn</td>
                    <td>
                        <input type="text" name="email" id="email" value="<?php if(isset($_COOKIE['email'])) echo $_COOKIE['email']; ?>"/>
                        <a style="color:red;"><?php echo isset($error['email']) ? $error['email'] : ''; ?></a>
                    </td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>
                        <input type="password" name="password" id="password" value="<?php if(isset($_COOKIE['pass'])) echo $_COOKIE['pass']; ?>"/>
                        <a style="color:red;"><?php echo isset($error['password']) ? $error['password'] : ''; ?></a>
                    </td>
                </tr>
                <tr>
                    <td>remember me</td>
                    <td><input type="checkbox" name="check" value="" <?php if(isset($_COOKIE['email'])) echo"checked"; ?> ></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="login" value="Login"/></td>
                </tr>
            </table>
        </form>
    </body>
</html>