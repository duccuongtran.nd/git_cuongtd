<?php 
    include_once './vendor/autoload.php';
    session_start(); 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Đăng ký</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/assets/css/docs.min.css">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-grid.css">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-reboot.css">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-grid.min.css">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-reboot.min.css">
    </head>
    <body>
    	<?php
            require_once('connect.php');
            $error = array();
            $data = array();
            if (!empty($_POST['login'])) {
                $data['email'] = $_POST['email'] ?? '';
                $data['password'] = $_POST['password'] ?? '';

                function is_email($email)
                {
                    return (!preg_match("/^[a-zA-Z0-9]{6,225}\@[a-zA-Z0-9]{3,20}\.[a-zA-Z]{2,5}$/", $email)) ? false : true;
                }
                
                function is_password($password)
                {
                    return (!preg_match("/^[a-zA-Z0-9]{6,50}$/", $password)) ? false : true;
                }
                if (empty($data['email'])) {
                    $error['email'] = 'Bạn chưa nhập email';
                } elseif (!is_email($data['email'])) {
                    $error['email'] = 'Email không đúng định dạng';
                }
                if (empty($data['password'])) {
                    $error['password'] = 'Bạn chưa nhập Password';
                } elseif (!is_password($data['password'])) {
                    $error['password'] = 'Password không đúng định dạng';
                }
                if (!$error) {
                    $email = $_POST["email"];
                    $password = $_POST["password"];
                    try {
                        $stmt = $conn->prepare("SELECT * FROM users where mail_address = '$email' and password = '$password' ");
                        $stmt->execute();
                    } catch (PDOException $ex) {
                        echo $ex->getMessage();
                    }
                    if (count($stmt->fetchAll(PDO::FETCH_ASSOC)) > 0) {
                        $_SESSION['email'] = $email;
                        if (isset($_POST['check'])) {
                            setcookie('email', $email, time() + 3600);
                            setcookie('pass', $password, time() + 3600);
                        }
                        header("location:LoginSuccessPdo.php");
                    } else {
                        echo 'Đăng nhập thất bại';
                    }
                } else {
                    echo 'Dữ liệu bị lỗi, không thể lưu trữ';
                }
            }
        ?>
        <form method="post" action="LoginPdo.php">
            <div class="container">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mail_address</label>
                        <input type="text" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="" value="<?php if(isset($_COOKIE['email'])) echo $_COOKIE['email']; ?>"/>
                        <a style="color:red;"><?php echo isset($error['email']) ? $error['email'] : ''; ?></a>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php if(isset($_COOKIE['pass'])) echo $_COOKIE['pass'] ?>"/>
                        <a style="color:red;"><?php echo isset($error['password']) ? $error['password'] : ''; ?></a>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input"  name="check" value="" <?php if(isset($_COOKIE['email'])) echo"checked"; ?> >
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>
                    <input type="submit" class="btn btn-primary" name="login" value="Login"/>
            </div>
        </form>
    </body>
</html>