<?php 

// Interface
Interface Boss
{
    public function checkValidSlogan();
}

// Trait
Trait Active
{
    public function defindYourSelf()
    {
        return get_class($this);
    }
}

// Bass class 
abstract class Supervisor implements Boss
{
    protected $slogan;

    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;
    }

    abstract function saySloganOutLoud();
}

// Children class
class EasyBoss extends Supervisor
{
    use Active;

    public function saySloganOutLoud()
    {
        return $this->slogan;
    }

    public function checkValidSlogan()
    {
        if (strpos($this->slogan, 'after') !== false || strpos($this->slogan, 'before') !== false ) {
            return true;
        }
        return false;
    }
}

class UglyBoss extends Supervisor
{
    use Active;

    public function saySloganOutLoud()
    {
        return $this->slogan;
    }

    public function checkValidSlogan()
    {
        if (strpos($this->slogan, 'after') !== false && strpos($this->slogan, 'before') !== false ) {
            return true;
        }
        return false;
    }
}

$easyBoss = new EasyBoss();
$uglyBoss = new UglyBoss();

$easyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s)');

$uglyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s). Only they can do it after check it all!');

$easyBoss->saySloganOutLoud(); 
echo "<br>";
$uglyBoss->saySloganOutLoud(); 

echo "<br>";

var_dump($easyBoss->checkValidSlogan());
echo "<br>";
var_dump($uglyBoss->checkValidSlogan());

echo "<br>";

echo 'I am ' . $easyBoss->defindYourSelf(); 
echo "<br>";
echo 'I am ' . $uglyBoss->defindYourSelf(); 
?>