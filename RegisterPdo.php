<?php 
    include_once './vendor/autoload.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Đăng ký</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/assets/css/docs.min.css">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-grid.css">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-reboot.css">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-grid.min.css">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-reboot.min.css">
    </head>
    <body>
        <?php
            require_once('connect.php');
            $error = array();
            $data = array();
            if (!empty($_POST['register'])) {
                $data['email'] = $_POST['email'] ?? '';
                $data['password'] = $_POST['password'] ?? '';
                $data['password_confirm'] = $_POST['password_confirm'] ?? '';
                function is_email($email)
                {
                    return (!preg_match("/^[a-zA-Z0-9]{6,225}\@[a-zA-Z0-9]{3,20}\.[a-zA-Z]{2,5}$/", $email)) ? false : true;
                }
                
                function is_password($password)
                {
                    return (!preg_match("/^[a-zA-Z0-9]{6,50}$/", $password)) ? false : true;
                }
                if (empty($data['email'])) {
                    $error['email'] = 'Bạn chưa nhập email';
                } elseif (!is_email($data['email'])) {
                    $error['email'] = 'Email không đúng định dạng';
                }
                if (empty($data['password'])) {
                    $error['password'] = 'Bạn chưa nhập Password';
                } elseif (!is_password($data['password'])) {
                    $error['password'] = 'Password không đúng định dạng';
                }
                if (empty($data['password_confirm'])) {
                    $error['password_confirm'] = 'Bạn cần nhập lại Password';
                } elseif (strpos($data['password'], $data['password_confirm']) === false) {
                    $error['password_confirm'] = 'Password chưa giống nhau';
                }
                if (!$error) {
                    $email = $_POST["email"];
                    $password = $_POST["password"];
                    try {
                        $stmt = $conn->prepare("INSERT INTO users (mail_address,password) VALUES (:mail_address,:password)");
                        $stmt->execute(array(':mail_address'=>$email, ':password'=>$password));
                        echo "Thêm dữ liệu thành công";
                    } catch (PDOException $ex) {
                        echo $ex->getMessage();
                    }
                } else {
                    echo 'Dữ liệu bị lỗi, không thể lưu trữ';
                }
            }
        ?>
        <form method="POST" action="RegisterPdo.php">
            <div class="container">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mail_address</label>
                        <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="<?php echo isset($data['email'])?>">
                        <a style="color:red;"><?php echo isset($error['email']) ? $error['email'] : ''; ?></a>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
                        <a style="color:red;"><?php echo isset($error['password']) ? $error['password'] : ''; ?></a>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password_confirm</label>
                        <input type="password" class="form-control" name="password_confirm" id="exampleInputPassword1" placeholder="Password_confirm">
                        <a style="color:red;"><?php echo isset($error['password_confirm']) ? $error['password_confirm'] : ''; ?></a>
                    </div>
                    <input type="submit" class="btn btn-primary" name="register" value="register"/>
            </div>
        </form>
    </body>
</html>