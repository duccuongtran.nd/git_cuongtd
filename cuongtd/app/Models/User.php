<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class User extends Model
{
    use SoftDeletes;
    protected $table = 'users';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'mail_address', 'name', 'address', 'phone', 'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function ListUsers()
    {
        $list = User::orderBy('mail_address', 'ASC')->paginate(20);
        return $list;
    }

    public function createUser(array $data)
    {
        $data['password'] = Hash::make($data['password']);
        return User::create($data);
    }

    public function getUser(array $data)
    {
        $users = User::orderBy('mail_address', 'ASC');
            if (isset($data['mail_address'])) {
                $users->where('mail_address', 'like','%'.$data['mail_address'].'%');
            }
            if (isset($data['name'])) {
                $users->where('name', 'like','%'.$data['name'].'%');
            }
            if (isset($data['address'])) {
                $users->where('address', 'like','%'.$data['address'].'%');
            }
            if (isset($data['phone'])) {
                $users->where('phone',$data['phone']);
            }
        return $users->paginate(20);
    }
}
