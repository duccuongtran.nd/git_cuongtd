<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('vi_VN');
        $limit = 100;
        DB::table('users')->truncate();
        for($i = 0; $i < $limit; $i++) {
            DB::table('users')->create([
                'name' => $faker->name,
                'mail_address' => $faker->unique()->email,
                'password' => $faker->password,
                'address' => $faker->address,
                'phone' => $faker->phoneNumber,
            ]);
        }
    }
}
