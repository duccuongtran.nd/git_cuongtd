@extends('layouts.default')

@section('title', 'Danh sách người dùng')

@section('header')
    <div class="row">
        @parent
        <p> Đây là trang danh sách người dùng </p> 
    </div>
@endsection

@section('content')
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4">
                <div class="jumbotrom">
                    <form method="get" action="{{ route('register.index') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <label>Email</label>
                        <div class="form-group">
                            <input type="text" name="mail_address" class="form-control" placeholder="Nhập mail cần tìm">
                        </div>
                        <label>Tên</label>
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Nhập tên cần tìm">
                        </div>
                        <label>Địa chỉ</label>
                        <div class="form-group">
                            <input type="text" name="address" class="form-control" placeholder="Nhập địa chỉ cần tìm">
                        </div>
                        <label>Phone</label>
                        <div class="form-group">
                            <input type="text" name="phone" class="form-control" placeholder="Nhập SDT cần tìm">
                        </div>
                        <div>
                            <input type="submit" name="search" value="Tìm kiếm" class="btn btn-lg btn-info">
                        </div>
                    </form>
                </div>
                </div>
            </div>
            </br>
            <div class="row">@include('flash::message')</div><br/>
                <h2>Danh sách</h2>
                <table class="table table-bordered">
                    <thead>
                        <tr style="text-align: center;">
                            <th>STT</th>
                            <th>Email</th>
                            <th>Tên</th>
                            <th>Địa chỉ</th>
                            <th>Số điện thoại</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $value)
                        <tr>
                            <td>{{(($users->currentPage() - 1 ) * $users->perPage() ) + $loop->iteration}}</td>
                            <td>{{$value->mail_address}}</td>
                            <td>{{ Helper::toUpperCase($value->name)}}</td>
                            <td>{{$value->address}}</td>
                            <td>{{$value->phone}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <a href="{{route('register.create')}}" class="btn btn-lg btn-info" >Thêm user</a>
            </div>
        </div>
    </div>
    {{ $users->links()}}
@endsection
