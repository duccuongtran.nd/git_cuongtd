<?php
/**
* check string
*
* @param string $values
* @return boolean
*/
function checkValidString($values) {
    //check $values is empty
    if (empty($values)) {
        return true;
    }
    if (strpos($values, 'after') !== false ) {
        return false;
    } 
    if (strpos($values, 'before' !== false ) || strlen($values) >= 50) {
        return true;
    }
    return false;
}

// get the file path
$path = 'file1.txt';
$path2 = 'file2.txt';
// open file
$fp = @fopen($path, "r");
$fp2 = @fopen($path2, "r");
// check the file has open successfully
if (!$fp || !$fp2) {
    if(!$fp) {
        echo "Mở file $path không thành công";
    } else {
        echo 'Mở file $path2 không thành công';
    }  
} else {
    // read the whole file
    $data = fread($fp, filesize('file1.txt'));
    $data2 = fread($fp2, filesize('file2.txt'));
    if (checkValidString($data)) {
        echo 'chuỗi hợp lệ';
    } else {
        echo 'chuỗi không hợp lệ';
    }
    echo "</br>";
    if (checkValidString($data2)) {
        echo 'chuỗi hợp lệ';
    } else {
        echo 'chuỗi không hợp lệ';
    }
}
?>